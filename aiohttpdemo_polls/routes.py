from views import index
from aiohttp import web


def setup_routes(app: web.Application) -> None:
    app.router.add_get('/', index)
