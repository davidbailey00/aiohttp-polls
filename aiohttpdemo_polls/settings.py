from typing import TypedDict
import pathlib
import yaml

BASE_DIR = pathlib.Path(__file__).parent.parent
config_path = BASE_DIR / 'config' / 'polls.yaml'


class PostgresConfig(TypedDict):
    database: str
    user: str
    password: str
    host: str
    port: int
    minsize: int
    maxsize: int


class Config(TypedDict):
    postgres: PostgresConfig


def get_config(path: pathlib.Path) -> Config:
    with open(path) as f:
        config: Config = yaml.safe_load(f)
    return config


config = get_config(config_path)
